* ./models/.. : Modeldefinitionen  
* ./initializers/sequelize.js : Initialisierung des ORM der Datenbank  
* ./actions/.. : Schnittstellen  
##
* ./config/routes.js : Konfiguration der Schnitstellenpfade  
* ./config/servers/web.js : Konfiguration des HTTP-Servers  
* ./config/sequelize.js : Konfiguration des ORM der Datenbank  
