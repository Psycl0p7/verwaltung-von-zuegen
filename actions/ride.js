const { Action, api } = require('actionhero')

exports.RideCreate = class RideCreate extends Action {
  constructor () {
    super()
    this.name = 'ride:create'
    this.inputs = {
      train: { required: true },
      platform: { required: true },
      departure: { required: true },
    }
    this.description = 'create a ride in database'
  }

  async run (data) {
    const ride = await api.models.ride.build(data.params)
    data.response.ride = ride.apiData(api)
  }
}

exports.RideList = class RideList extends Action {
  constructor () {
    super()
    this.name = 'ride:list'
    this.inputs = {}
    this.description = 'list all rides'
  }

  async run (data) {
    const ride = await api.models.ride.findAll({
      order: [
        'departure', 'DESC'
      ]
    })
    if (!ride) { throw new Error('ride not found') }
    data.response.ride = ride.apiData(api)
  }
}

exports.RideEdit = class RideEdit extends Action {
  constructor () {
    super()
    this.name = 'ride:edit'
    this.description = 'edit a ride in database'
    this.inputs = {
      id: { required: true },
      train: { required: true },
      platform: { required: true },
      departure: { required: true },
    }
  }

  async run (data) {
    const ride = await api.models.ride.findOne({ where: { id: data.params.id } })
    if (!ride) { throw new Error('ride not found') }

    await ride.updateAttributes(data.params)
    data.response.ride = ride.apiData(api)
  }
}
