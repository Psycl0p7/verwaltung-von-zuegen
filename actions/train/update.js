const { Action, api } = require('actionhero')

module.exports = class TrainUpdate extends Action {
  constructor () {
    super()
    this.name = 'train:update'
    this.description = 'edit a train'
    this.inputs = {
      id: { required: true },
      train: { required: true },
      platform: { required: true },
      departure: { required: true },
    }
  }

  async run (data) {
    const train = await api.models.train.findOne({ where: { id: data.params.id } })
    if (!train) { throw new Error('train not found') }

    await train.updateAttributes(data.params)
    data.response.train = train.apiData(api)
  }
}