const { Action, api } = require('actionhero')

module.exports = class TrainCreate extends Action {
  constructor () {
    super()
    this.name = 'train:create'
    this.description = 'create a train in database'
    this.inputs = {
      train: { required: true },
      platform: { required: true },
      departure: { required: true },
    }
  }

    async run (data) {
    const train = await api.models.train.build(data.params)
    data.response.train = train
  }
}
