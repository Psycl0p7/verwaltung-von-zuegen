const { Action, api } = require('actionhero')

module.exports = class TrainList extends Action {
  constructor () {
    super()
    this.name = 'train:list'
    this.description = 'list all trains'
    this.inputs = {}
  }

  async run (data) {
    const train = await api.models.train.findAll({
      order: [
        'departure', 'DESC'
      ]
    })
    if (!train) { throw new Error('train not found') }
    data.response.train = train.apiData(api)
  }
}
