const { Action, api } = require('actionhero')

exports.PlatformList = class PlatformList extends Action {
    constructor () {
      super()
      this.name = 'platform:list'
      this.inputs = {}
      this.description = 'list all platforms'
    }
  
    async run (data) {
      const platforms = await api.models.platform.findAll({
        order: [
          'departure', 'DESC'
        ]
      })
      if (!platforms) { throw new Error('ride not found') }
      data.response.platform = platforms.apiData(api)
    }
  }
  