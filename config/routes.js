exports.default = {
  routes: function (api) {
    return {

      get: [
        { path: '/train/list', action: 'train:list' },
        { path: '/ride/list', action: 'ride:list' },
        { path: '/platform/list', action: 'platform:list' },
      ],
      
      post: [
        { path: '/train/create', action: 'train:create' },
        { path: '/ride/create', action: 'ride:create' },
      ],
      
      put: [
        { path: '/train/update', action: 'train:update' },
        { path: '/ride/update', action: 'ride:update' },
      ],
    }
  }
}
