exports.default = {
  sequelize: function (api) {
    return {
      dialect: 'sqlite',
      storage: 'database.sqlite',
      logging: true
    }
  }
}

exports.development = exports.default.sequelize()
