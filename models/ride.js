module.exports = function (sequelize, DataTypes) {
    const Ride = sequelize.define('Ride', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true
      },
      departure: {
        type: DataTypes.DATE,
        allowNull: false,
      }
    })

    Ride.associate = function(models) {
      Train.belongsTo(models.ride)
      Train.belongsTo(models.platform)
    }

    return Ride
  }

  