module.exports = function (sequelize, DataTypes) {
  const Train = sequelize.define('Train', {
    id: {
      type: DataTypes.UUID,
      defaultValue: sequelize.UUIDV4,
      allowNull: false,
      unique: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    destination: {
      type: DataTypes.STRING,
    },
  })

  Train.associate = (models) => {
    Train.belongsToMany(models.ride)
  }

  return Train
}

/*

  train.belongsToMany(ride)
  train.belongsTo(platform)

  Train.prototype.name = function () {
    return [this.firstName, this.lastName].join(' ')
  }

  Train.prototype.updatePassword = async function (password) {
    const hash = await bcrypt.hash(password, bcryptSaltRounds)
    this.passwordHash = hash
  }

  Train.prototype.checkPassword = async function (password, callback) {
    return bcrypt.compare(password, this.passwordHash)
  }

  Train.prototype.apiData = function (api) {
    return {
      id: this.id,
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName
    }
  }
*/
