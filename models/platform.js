module.exports = function (sequelize, DataTypes) {
    const Platform = sequelize.define('Platform', {
      id: {
        type: DataTypes.UUID,
        defaultValue: sequelize.UUIDV4,
        allowNull: false,
        unique: true,
        primaryKey: true
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      lengthM: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    })

    /*
    Platform.associate = (models) => {
        Platform.belongsToMany(models.ride)
    }
    */

    return Platform
}
  